<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/jsp/header.jsp" />
		<title>Etape 3</title>
	</head>
	<body>
		<h1 class="content-title">Etape 3 : File Management</h1>
		<br><br>
		<div class="container">
			<div class="row justify-content-center">
				Désolé Billy, on dirait que cette étape se passe uniquement en Back.<br>
				Si tu veux voir le résultat de ce que tu viens de faire: Go to Etape 1.
			</div>
		</div>
	</body>
</html>